// console.log("hello world");
// [SECTION] Arithmetic Operators
    // allow us to perform mathematical operations between operands (value ong either sides of the operator).
    // it returns a numerical value


let x = 100;
let y = 25;
let sum = x + y;
console.log("Result of addition operator : " + sum);

let difference = x - y;
console.log("Result of subtraction operator : " + difference);

let product = x * y;
console.log("Result of multiplication operator : " + product);

let quotient = x / y;
console.log("Result of division operator : " + quotient);

let mod = x % y;
console.log("Result of modulo operator : " + mod);


// Assignment operator
    // Assigns the value of the right operand to a variable
// Basic assignment operator
let assignmentNumber = 8;
console.log(assignmentNumber);

// assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;

console.log("Result of addition assignment operator :" + assignmentNumber);

// Subtraction/Multiplication>Division Assignment operator (-=, *=, /=)
assignmentNumber -= 2;

console.log("Result of subtraction assignment operator :" + assignmentNumber);

assignmentNumber *= 2;

console.log("Result of multiplication assignment operator :" + assignmentNumber);

assignmentNumber /= 2;

console.log("Result of division assignment operator :" + assignmentNumber);

assignmentNumber %= 2;

console.log("Result of modulo assignment operator :" + assignmentNumber);

// PEMDAS (Order of Operations)
// MDAS
// Multiple Operators or parenthesis
let mdas =1 + 2 -3 *4 /5
console.log("Result of MDAS Operation: " + mdas);

// Order of operations can be changed by adding a parenthesis

let pemdas = 1+(2-3)*(4/5);
console.log("Result of PEMDAS Operation: " + pemdas);

pemdas = (1+(2-3))*(4/5);
console.log("Result of PEMDAS Operation: " + pemdas);

// Increment and Decrement
    // This operators add or subtract values by 1 and reassign the valuie of the variables where the  increment/decrement was applied
let z = 1;
    // Increment (++);
    let increment = ++z;
    console.log("Result of Pre-increment: " + increment);
    console.log("Result of Pre-increment: " + z);

    increment = z++;
    console.log("Result of Post-increment: " + increment);
    console.log("Result of Post-increment: " + z);

    // Decrement (--)
    let decrement = --z;
    console.log("Result of Pre-decrement: " + decrement);
    console.log("Result of Pre-decrement: " + z);

    increment = z--;
    console.log("Result of Post-decrement: " + decrement);
    console.log("Result of Post-decrement: " + z);

// Type Coercion
    // automatic or implicit conversion of value from one data type or another.
    // This happens when operations are performed on different data types that would normally not be possible and yield irregular results.
    // "Automatic Conversion"

    let numA = '10';
    let numB = 12;

    // performing addition operation to a number and a string variable will result into concatenation
    let coercion = numB + numA;
    console.log(coercion);
    console.log(typeof coercion);

    let numC = 16;
    let numD = 14;

    let nonCoercion = numC + numD;
    console.log(nonCoercion);
    console.log(typeof nonCoercion);
    
    // Boolean true is also associated with the the value of 1.
    // Boolean false is also associated with the the value of 0.

    let numE = true + 1;
    console.log(numE);

    let numF = false + 1;
    console.log(numF);

// Comparison Operator
    // are used to evaluate and compare the left and right operands.
    // it returns a boolean value.

    // Equality Operator (==)

    let juan = 'juan';
    console.log("Equality Operator");
    console.log(1==1);
    console.log(1==2);
    console.log(1=='1');
    console.log(false==0);
    console.log("juan"=='juan');
    console.log("juan"=='Juan');
    console.log(juan=='juan');


    // Inequality Operator (!=)
    console.log("Inequality Operator")
    console.log(1!=1);
    console.log(1!=2);
    console.log(1!='1');
    console.log(false!=0);
    console.log("juan"!='juan');
    console.log("juan"!='Juan');
    console.log(juan!='juan');

    // Strictly Equality Operator ( === )
    console.log("Strictly Equality Operator: ");
    console.log(1 === 1);
    console.log(1 === 2);
    console.log(1 === '1');
    console.log(false === 0);
    console.log('juan' === "juan");
    console.log('juan' === "Juan");
    console.log(juan === "juan");

    // Strictly Inequality Operator ( === )
    console.log("Strictly Inequality Operator: ");
    console.log(1 !== 1);
    console.log(1 !== 2);
    console.log(1 !== '1');
    console.log(false !== 0);
    console.log('juan' !== "juan");
    console.log('juan' !== "Juan");
    console.log(juan !== "juan");

// Greater Than and Less Than Operator
    // Some comparison operators check whether one value is greater or less than to the other value
    // Returns a boolean value

    let a = 50;
    let b = 65;

    // GT or Greater than (>)
    console.log("Greater Than or Less Than");
    let isGreaterThan = a > b;
    console.log(isGreaterThan);
    
    // LT or Less than (<)
    let isLessThan = a < b;
    console.log(isLessThan);

    // GTE or Greater than or equal (>=)
    b = 50;
    let isGTorEqual = a >= b;
    console.log(isGTorEqual);

    // LTE or Less than or equal (<>>=)
    a = 66;
    let isLTorEqual = a <= b;
    console.log(isLTorEqual);

    let numStr = "30";
    console.log (a <numStr);
    // -true = forced coercion to change string into number.

    let strNum = "twenty";
    console.log(b>strNum);
    //what ever operator is used it will be result to false unless the string is numeric

// Logical operators 
    // to allow us for a more specific logical combination of conditions and evaluations 
    // It returns a boolean value.
    let isLegalAge = false;
    let isRegistered = true;
    
    // Logical And Operator ( && - Double Ampersand )
    let allRequirementsMet = isLegalAge && isRegistered;
    console.log("result of logical and operator : "+ allRequirementsMet);
       
    // Logical OR Operator

    let someRequarementsMet = isLegalAge || isRegistered;

    console.log("result of logical or operator : " + someRequarementsMet);

    // Logical NOT operator (!- exclamation mark)

    console.log("result o logical not operator : " + !isRegistered);
    console.log("result o logical not operator : " + !isLegalAge);